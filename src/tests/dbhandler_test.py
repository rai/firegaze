'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
       
Created on 14 Nov 2012

@author: Rijnard
'''
import unittest
from firegaze.util.dbhandler import DBHandler

class Test(unittest.TestCase):


    def setUp(self):
        self.dbhandler = DBHandler('dailytest', 'monthlytest')
        
        doc = {"IP" : "192.168.128.1"}
        self.dbhandler.daily_in_collection.update(doc, doc, upsert=True)
        self.dbhandler.monthly_in_collection.update(doc, doc, upsert=True)

    def tearDown(self):
        pass

    def test_clear_collections(self):
        result_day = [x for x in self.dbhandler.daily_in_collection.find()]
        result_month = [x for x in self.dbhandler.monthly_in_collection.find()]
        self.assertTrue(result_day)
        self.assertTrue(result_month)
        
        self.dbhandler.clear_collections()
        result_day = [x for x in self.dbhandler.daily_in_collection.find()]
        result_month = [x for x in self.dbhandler.monthly_in_collection.find()]
        self.assertFalse(result_day)
        self.assertFalse(result_month)
    
    def test_drop_collections(self):
        collection_day = self.dbhandler.db['dailytest_in']
        collection_month = self.dbhandler.db['monthlytest_in']
        self.assertEquals(1, collection_day.count())
        self.assertEquals(1, collection_month.count())
        
        self.dbhandler.drop_collections()
        
        collection_day = self.dbhandler.db['dailytest_in']
        collection_month = self.dbhandler.db['monthlytest_in']
        self.assertEquals(0, collection_day.count())
        self.assertEquals(0, collection_month.count())
        