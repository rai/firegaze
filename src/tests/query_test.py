'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.

Created on 01 Nov 2012

@author: Rijnard
'''
# pylint: disable-msg=R0904, C0103

import unittest
from firegaze.util.dbhandler import DBHandler
from datetime import datetime, time
from firegaze.util.query import FWLogQuery
from firegaze.util.config import conf


class QueryTest(unittest.TestCase):
    '''
    Unit test suite for testing util.query
    '''

    def setUp(self):
        '''
        Connect with MongoDB and populate collection with dummy values
        '''
        self.INSERT_DATE = datetime(2012, 1, 1, 12)
        self.INSERT_DAY_DATE = datetime.combine(datetime(2012, 1, 1, 12),
                                                time.min)
        self.dbhandler = DBHandler('dailytest', 'monthlytest')
        conf.DAILY_DB_NAME = 'dailytest'
        conf.MONTHLY_DB_NAME = 'monthlytest'
        
        daily_query = {'_id': 1,
                           'date': self.INSERT_DAY_DATE,
                           'IP': '1.1.1.1'
                           }
        daily_update = { '$inc': {'hourly.%d.%s.%d' % (12, 'ports', 80): 1,
                                      'hourly.%d.%s.%s' % (12, 'ports', 'total') : 1,
                                      'hourly.%d.%s.%s' % (12, 'srcs', "2-2-2-2") : 1,
                                      'minute.%d.%d.%s.%d' % (12, 0, 'ports', 80): 1,
                                      'minute.%d.%d.%s.%s' % (12, 0, 'ports', 'total') : 1,
                                      'minute.%d.%d.%s.%s' % (12, 0, 'srcs', "2-2-2-2") : 1 }}
        
        monthly_query = {'_id': 1,
                             'date': self.INSERT_DAY_DATE.replace(day=1),
                             'IP': '1.1.1.1'
                             }
        
        monthly_update = {'$inc' : {'daily.%d.%s.%d' % (1, 'ports', 80): 1,
                                        'daily.%d.%s.%s' % (1, 'ports', 'total') : 1,
                                        'daily.%d.%s.%s' % (1, 'srcs', '2-2-2-2') : 1}}
        
        self.dbhandler.daily_in_collection.update(daily_query, 
                                                  daily_update, upsert=True)
        self.dbhandler.monthly_in_collection.update(monthly_query, 
                                                    monthly_update, upsert=True)

        self.query = FWLogQuery()
        
    def test_get_most_data_ports(self):
        '''
        Test retrieving packets by ports, sorted in descending order
        '''
        counts = self.query.most_data_ports(self.INSERT_DATE)
        self.assertEqual(counts, (('80',), (1,)))
        
    def test_get_highest_packet_counts(self):
        '''
        Test retrieving IPs with highest packet counts
        '''
        counts = self.query.highest_day_packet_counts(self.INSERT_DATE)
        self.assertTupleEqual(counts, (['1.1.1.1'], [1]))
        
        counts = self.query.highest_hour_packet_counts(self.INSERT_DATE)
        self.assertTupleEqual(counts, (['1.1.1.1'], [1]))
        
        counts = self.query.highest_minute_packet_counts(self.INSERT_DATE)
        self.assertTupleEqual(counts, (['1.1.1.1'], [1]))
        
    def test_get_highest_port_counts(self):
        '''
        Test retrieving IPs with highest port counts
        '''
        counts = self.query.highest_day_port_counts(self.INSERT_DATE)
        self.assertTupleEqual(counts, (('1.1.1.1',), (1,)))
        
        counts = self.query.highest_hour_port_counts(self.INSERT_DATE)
        self.assertTupleEqual(counts, (('1.1.1.1',), (1,)))
        
        counts = self.query.highest_minute_port_counts(self.INSERT_DATE)
        self.assertTupleEqual(counts, (('1.1.1.1',), (1,)))
        
    def test_get_port_counts(self):
        '''
        Test retrieving port counts
        '''
        counts = self.query.get_day_port_counts(['1.1.1.1'],
                                                date=self.INSERT_DATE)
        self.assertListEqual(counts, [1])
        
        counts = self.query.get_hour_port_counts(['1.1.1.1'],
                                                 date=self.INSERT_DATE)
        self.assertListEqual(counts, [1])
        
        counts = self.query.get_minute_port_counts(['1.1.1.1'],
                                                   date=self.INSERT_DATE)
        self.assertListEqual(counts, [1])
        
    def test_get_packet_counts(self):
        '''
        Test retrieving packet count totals
        '''
        counts = self.query.get_day_packet_counts(['1.1.1.1'],
                                                  date=self.INSERT_DATE)
        self.assertListEqual(counts, [1])
        
        counts = self.query.get_hour_packet_counts(['1.1.1.1'],
                                                   date=self.INSERT_DATE)
        self.assertListEqual(counts, [1])
        
        counts = self.query.get_minute_packet_counts(['1.1.1.1'],
                                                     date=self.INSERT_DATE)
        self.assertListEqual(counts, [1])
        
        counts = self.query.get_day_packet_counts(['255.255.255.255'],
                                                  date=self.INSERT_DATE, port=1)
        self.assertListEqual(counts, [0])
        
    def test_get_ips(self):
        '''
        Test retrieving IPs from regex
        '''
        ip = self.query.get_ips('1\.1\.1\.1')
        self.assertListEqual(ip, ['1.1.1.1'])
        
    def test_get_packet_counts_by_srcs(self):
        '''
        Test retrieving packet counts by IP sources
        '''
        srcs = self.query.get_day_packet_counts_by_srcs('1.1.1.1',
                                                        date=self.INSERT_DATE)
        self.assertListEqual(srcs, [('2-2-2-2', 1)])
        
        srcs = self.query.get_hour_packet_counts_by_srcs('1.1.1.1',
                                                         date=self.INSERT_DATE)
        self.assertListEqual(srcs, [('2-2-2-2', 1)])
        
        srcs = self.query.get_minute_packet_counts_by_srcs('1.1.1.1',
                                                           date=self.INSERT_DATE)
        self.assertListEqual(srcs, [('2-2-2-2', 1)])
        
    def test_get_packet_counts_by_port(self):
        '''
        Test retrieving packet counts by port
        '''
        ports = self.query.get_day_packet_counts_by_port('1.1.1.1',
                                                         date=self.INSERT_DATE)
        self.assertListEqual(ports, [('80', 1), ('total', 1)])
        
        ports = self.query.get_hour_packet_counts_by_port('1.1.1.1', 
                                                          date=self.INSERT_DATE)
        self.assertListEqual(ports, [('80', 1), ('total', 1)])
        
        ports = self.query.get_minute_packet_counts_by_port('1.1.1.1',
                                                            date=self.INSERT_DATE)
        self.assertListEqual(ports, [('80', 1), ('total', 1)])

    def tearDown(self):
        '''
        Drop the collection
        '''
        self.dbhandler.drop_collections()

#suite = unittest.TestLoader().loadTestsFromTestCase(QueryTest)
#unittest.TextTestRunner(verbosity=2).run(suite)
