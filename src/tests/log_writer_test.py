'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
       
Created on 01 Nov 2012

@author: Rijnard
'''
# pylint: disable-msg=W0212, R0904, C0103

import unittest
from firegaze.log import log_writer
import datetime


class LogWriterTest(unittest.TestCase):
    '''
    Tests that the LogWriter correctly parses and makes documents
    for database insertion
    '''


    def setUp(self):
        self.log_line = """
        Nov  1 15:25:15 ubuspons kernel: [16479.817936] FWLOG=OUT\
         IN= OUT=eth0 SRC=10.0.2.15 DST=146.232.20.10 LEN=60 TOS=0x00\
         PREC=0x00 TTL=64 ID=0 DF PROTO=UDP SPT=2666 DPT=53 LEN=40
        """
        
        self.logWriter = log_writer.LogWriter(None, None)

    def tearDown(self):
        pass


    def test_log_to_dict(self):
        '''
        Test that a log line is parsed correctly to a dictionary
        '''
        dic = log_writer._log_to_dict(self.log_line)
        
        keys = ['SRC', 'DPT', 'PROTO', 'TOS', 'DST', 'TTL', 
                  'SPT', 'LEN', 'FWLOG', 'PREC', 'IN', 'ID', 'OUT']
        
        values = ['10.0.2.15', '53', 'UDP', '0x00', '146.232.20.10',
                  '64', '2666', '40', 'OUT', '0x00', '', '0', 'eth0']
        
        self.assertListEqual(dic.keys(), keys)
        self.assertListEqual(dic.values(), values)
        
    def test_make_document(self):
        '''
        Test that a MongoDB document dictionary is correctly generated for insertion
        '''
        dic = log_writer._log_to_dict(self.log_line)
        
        doc = self.logWriter.make_document('DST', 'DPT', dic)
        
        doc0 = {'date': datetime.datetime(2012, 11, 1, 0, 0), 
                'IP': '146.232.20.10', 
                '_id': '146.232.20.10/20121101'}
        
        self.assertDictEqual(doc[0], doc0)
        
        doc1 = {'$inc': {'daily.1.srcs.10-0-2-15': 1, 
                         'daily.1.ports.53': 1, 
                         'daily.1.ports.total': 1}}
        self.assertDictEqual(doc[3], doc1)
        
#suite = unittest.TestLoader().loadTestsFromTestCase(LogWriterTest)
#unittest.TextTestRunner(verbosity=2).run(suite)