'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
       
Created on 11 Sep 2012

@author: Rijnard
'''
from setuptools import setup, find_packages
import glob

setup(
      name = "Firegaze",
      version = "1.2",
      author = "Rijnard van Tonder",
      author_email = "rvantonder@gmail.com",
      description = "A distributed firewall monitoring solution",
      license = "Apache License (2.0)",
      keywords = "firewall logging iptables",
      packages = find_packages(),
      url = "https://bitbucket.org/rai/firegaze/overview",

      install_requires = [], 
      test_suite = "tests",
      data_files = [('/etc/firegaze/', ['conf/firegaze.conf']),
                    ('/etc/rsyslog.d/', ['conf/10-iptables.conf']),
                    ('/etc/init.d/', ['conf/firegaze']),
                    ('/var/www/firegaze/', glob.glob('web/live/*.php')),
                    ('/var/www/firegaze/', glob.glob('web/template/*.php')),
                    ('/var/www/firegaze/js', glob.glob('web/js/*'))
                   ],
      classifiers = [
                     "Development Status :: 3 - Alpha",
                     "Intended Audience :: System Administrators"
                     "Topic :: Utilities",
                     "License :: OSI Approved :: Apache Software License",
                     ],
      entry_points={
                    'console_scripts':
                        ['firegazequery = firegaze.scripts.query_logs:main', 
                         'firegazecapture = firegaze.scripts.capture_logs:main']
                    }
      )
