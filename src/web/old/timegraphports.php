<html>
<head>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>

<script type="text/javascript" src="js/highslide-full.min.js"></script>
<script type="text/javascript" src="js/highslide.config.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="js/highslide.css" />

<link rel="stylesheet" media="all" type="text/css" href="js/jquery-ui.css" />
<link rel="stylesheet" media="all" type="text/css" href="js/jquery-ui-timepicker-addon.css" />

<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>

<style type="text/css">
.ui-widget { font-size: .8em;}
</style>

<a href="all_ports.php">Go to Live Dash</a>

<?php

date_default_timezone_set('Africa/Johannesburg');

if (isset($_GET['ip'])) {
  $IP = $_GET['ip'];

  if (isset($_GET['datefrom']) && !empty($_GET['datefrom'])) {
    $DATEFROM = $_GET['datefrom'];
    $DATEFROM = date("Y-m-d", strtotime($DATEFROM)); # ALWAYS USE A ROUND NUMBER TO ALIGN WITH GRAPH
    echo '<h3>'.$DATEFROM.'</h3>';
  } else {
    $TMPDATE = date("Y-m-d H:i");
    $DATEFROM = strtotime( '-1 day', strtotime($TMPDATE)); # DEFAULTS TO 1 DAY PRIOR
    $DATEFROM = date("Y-m-d", $DATEFROM); # USE A ROUND NUMBER TO ALIGN WITH GRAPH
    echo '<h3>'.$DATEFROM.'</h3>';
   } 

  if (isset($_GET['dateto']) && !empty($_GET['dateto'])) {
    $DATETO = $_GET['dateto'];
    echo '<h3>'.$DATETO.'</h3>';
  } else {
    $TMPDATE = date("Y-m-d H:i");
    $DATETO = strtotime( '+1 day', strtotime($TMPDATE));
    $DATETO = date("Y-m-d H:i", $DATETO); // now (forward one day)
    echo '<h3>'.$DATETO.'</h3>';
  }

  if (isset($_GET['gran']) && !empty($_GET['gran'])) {
    $GRAN = $_GET['gran']; // should be min, hour, or day
  } else {
    die('<h2>Specify acceptable granularity</h2>');
  }

  if (isset($_GET['direction']) && !empty($_GET['direction'])) {
    $DIRECTION = $_GET['direction']; // should be o or i
    
  } else {
    die('<h2>Specify acceptable direction</h2>');
  }

  if ($GRAN == "day") {
    $res = 24 * 3600;
  } elseif ($GRAN == "hour") {
    $res = 3600;
  } elseif ($GRAN == "min") {
    $res = 60;
  }

  echo '<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>';
  ?>

  <script type="text/javascript">
    $(function() {
      $(document).ready(function () {
        $.datepicker.setDefaults({
          dateFormat: 'yy-mm-dd'
        });
      $('#date_from_input').datetimepicker();
      $('#date_to_input').datetimepicker();
      }); 
    });
  </script>

  <form action="" method="get">
  IP: <input type="text" name="ip" value="<?php echo $IP; ?>" />
  Date From: <input type="text" name="datefrom" id="date_from_input" value="<?php echo $DATEFROM; ?>" />
  Date To: <input type="text" name="dateto" id="date_to_input" value="<?php echo $DATETO; ?>" /><br>
  <input type="radio" name="gran" value="day" <?php if ($GRAN == "day") { echo 'checked'; } ?>/>Day
  <input type="radio" name="gran" value="hour" <?php if ($GRAN == "hour") { echo 'checked'; } ?>/>Hour
  <input type="radio" name="gran" value="min" <?php if ($GRAN == "min") { echo 'checked'; } ?>/>Min
  <input type="radio" name="direction" value="i" <?php if ($DIRECTION == "i") { echo 'checked'; } ?>/>In
  <input type="radio" name="direction" value="o" <?php if ($DIRECTION == "o") { echo 'checked'; } ?>/>Out
  <input type="submit" value="Submit" />
  </form>

  <script type="text/javascript">
  $(function () {
    var chart;
    $(document).ready(function() {
        // get alert level
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                zoomType: 'x',
                spacingRight: 20,
                events: {
                  click: function(e) {
                    this.yAxis[0].removePlotLine('dangerline');
                    var y = e.yAxis[0].value;
                    this.yAxis[0].addPlotLine({
                      value: y,
                      color: 'red',
                      width: 2,
                      id: 'dangerline',
                      dashStyle: 'shortdash',
                      label: {
                        text: 'Alert level'
                      }
                    });

                    $.getJSON('setthresh.php', {val : y, gran : "<?php echo $GRAN; ?>", ip : "<?php echo $IP ?>", direction : "<?php echo $DIRECTION ?>"}, function(data) {
                      alert("Test affirmative: "+data.rval);
                    }).error(function() { alert("error"); }); 
                  }
                },
                resetZoomButton: {
                  position: {
                    x: -50,
                    y: -45 
                  },
                },
            },
            title: {
                text: 'Firewall Traffic'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' :
                    'Drag your finger over the plot to zoom in'
            },
            xAxis: {
                type: 'datetime',
                maxZoom: <?php echo $res; ?> * 1000, // fourteen days
                plotLines: [{
                  id : "currenttime",
                  value: <?php echo (time()+7200)*1000;?>, // TODO utc time
                  color: 'blue',
                  width: 2,
                  label : {
                    text: 'Current Time'
                  }
                }],
                title: {
                    text: null
                }
            },
            yAxis: {
                title: {
                    text: '# Packets'
                },
                //plotLines : [{
                //  id : "alertlevel",
                //  value: alertlevel,
                //  color : 'red',
                //  dashStyle : 'shortdash',
                //  width: 2,
                //  label : {
                //    text: 'Stranger danger'
                //  }
                //}],
                min: 0.6,
                startOnTick: false,
                showFirstLabel: false,
                maxPadding: 1.3 
            },
            tooltip: {
                shared: false
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, 'rgba(2,0,0,0)']
                        ]
                    },
                    lineWidth: 1,
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: true,
                                radius: 5
                            }
                        }
                    },
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    }
                },
                series: {
                  cursor: 'pointer',
                  point: {
                    events: {
                      click: function() {
                        var gmttime = this.x/1000; 
                        var offset = new Date().getTimezoneOffset() * 60;
                        var localtime = gmttime + offset;
                        // $("#portbreakdown").html("<p>a</p>");
                        window.location.pathname = 'firegaze/portbreakdown.php?gran=<?php echo $GRAN ?>&ip='+this.series.name+'&direction=<?php echo $DIRECTION;?>&date='+localtime;
                        hs.htmlExpand(null, {
                          pageOrigin: {
                            x: this.pageX,
                            y: this.pageY
                          },
                          maincontentId: 'portbreakdown',
                          width: 200
                        });
                      }
                    }
                  },
                  marker: {
                    lineWidth: 1
                  }
               }
            },
    
            series: [
                    <?php $all_ips = exec('/usr/local/bin/firegazequery --listips -'.$DIRECTION.' --date "'.$DATEFROM.'" --ips "^'.$IP.'"');
                          foreach (json_decode($all_ips) as $value) {
                            echo "{";
                            echo "type: 'line',";
                            ?>
                            marker: { symbol: 'circle', radius: 0 }, 
                            <?
                            echo "name: '$value',";
                            echo "pointInterval: $res * 1000,";
                            $DFS = strtotime($DATEFROM);
                            echo "pointStart: Date.UTC(".date("Y", $DFS).', '.(intval(date("m", $DFS))-1).', '.date("d", $DFS)."),"; 
                            $ip_counts = exec('/usr/local/bin/firegazequery --histplot --port -'.$DIRECTION.' --hg '.$GRAN.' --hstart "'.$DATEFROM.'" --hend '.$DATETO.' --ips "^'.$value.'"');
                            echo "data: $ip_counts";
                            echo "},";
                          }
                          echo "],";
                    ?>
        });
    });
  });
  </script>
<?
} else {
  ?>
  <h3>No IP specified.</h3>
  <?
} ?>

<h3><?php echo '/usr/local/bin/firegazequery --listips -'.$DIRECTION.' --date "'.$DATEFROM.'" --ips "^'.$IP.'"'; ?></h3>
<h1><?php echo $IP; ?></h1>
<h1><?php echo $all_ips; ?></h1>


<div id="portbreakdown" class="highslide-maincontent">
</div>
