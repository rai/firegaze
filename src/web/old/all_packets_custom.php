<html>
<head>
<body>
   
<link rel="stylesheet" media="all" type="text/css" href="js/jquery-ui.css" />
<link rel="stylesheet" media="all" type="text/css" href="js/jquery-ui-timepicker-addon.css" />

<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>

<script type="text/javascript" src="js/jquery.dataTables.js"></script>

<style type="text/css">
  @import "js/demo_page.css";
  @import "js/demo_table.css";
a{ color: #330000; }
.ui-widget { font-size: 1em;}
.dataTables_wrapper {
  float: left;
  position: relative;
  width: 49%;
  margin: 5px;
  clear: none
}

#dt_example {
  margin: 0px auto;
  width:1400px
}
</style>

<?php 

date_default_timezone_set('Africa/Johannesburg');

if (isset($_GET['date'])) {
  $headline = $_GET['date'];
  $date = $_GET['date'];
} else {
  $headline = "Current Time";
  $date = date("Y-m-d H:i");
?>
  <script type="text/javascript">
    window.onload = setupRefresh;

    function setupRefresh() {
      setTimeout("refreshPage();", 30000);
    }
    function refreshPage() {
      window.location = location.href;
    }
  </script>
<?
}
?>

<hr>
<?php
  if ($headline == "Current Time") {
    echo '<center>';
    echo '<font face="Arial"  size="+3" color="#FF2222">LIVE</font>';
    echo '</center>';
  } else {
    echo '<a href="all_packets.php">Go to Live Dash</a>';
  }
?>

<h2 align=center>Packet Dash</h2>

<h3 align=center>Packets for <?php echo $headline ?></h3>

<script type="text/javascript">
  $(function () {
   $(document).ready(function() {
    $.datepicker.setDefaults({
      dateFormat: 'yy-mm-dd'
    });
    $('#date_input').datetimepicker();
   });
  });
</script>

<center>
<form action="" method="get"> <!-- Empty form action for current page-->
Date: <input type="text" name="date" id="date_input" value="<?php echo $date ?>" />
<input type="submit" value="Submit" />
</form>
</center>

<script type="text/javascript" charset="utf-8">
  $(document).ready(function () {
    $('#day_in').dataTable({
      "iDisplayLength": 10,
      "aaSorting": [[1, "desc"]] });
    $('#day_out').dataTable({
      "iDisplayLength": 10,
      "aaSorting": [[1, "desc"]] });
    $('#hour_in').dataTable({
      "iDisplayLength": 10,
      "aaSorting": [[1, "desc"]] });
    $('#hour_out').dataTable({
      "iDisplayLength": 10,
      "aaSorting": [[1, "desc"]] });
    $('#minute_in').dataTable({
      "iDisplayLength": 10,
      "aaSorting": [[1, "desc"]] });
    $('#minute_out').dataTable({
      "iDisplayLength": 10,
      "aaSorting": [[1, "desc"]] });
  });
</script>

<h1>MINUTE</h1>

<body id="dt_example">

<table cellpadding="0" cellspacing="0" class="display" id="minute_in" border="1"> 
<thead>
<tr>
  <th>IP</th><th># Packets INBOUND</th>
</tr>
</thead>
<tbody>
<?php
  $json_object = exec('/usr/local/bin/firegazequery --dbprefix "sotm" --data -i -g min --date ' . '"' . $date . '"' );
  $json_decoded = json_decode($json_object);

  foreach($json_decoded as $val) {
    echo "<tr class=\"ip\">\n";
    echo "<td><a href=\"timegraphdata_custom.php?ip=$val[0]&datefrom=$date&gran=min&direction=i\">$val[0]</a></td><td>$val[1]</td>\n";
    echo "</tr>\n";
  }
?>
</tbody>
</table>

<table cellpadding="0" cellspacing="0" class="display" id="minute_out" border="1"> 
<thead>
<tr>
  <th>IP</th><th># Packets OUTBOUND</th>
</tr>
</thead>
<tbody>
<?php
  $json_object = exec('/usr/local/bin/firegazequery --dbprefix "sotm" --data -o -g min --date ' . '"' . $date . '"' );
  $json_decoded = json_decode($json_object);

  foreach($json_decoded as $val) {
    echo "<tr class=\"ip\">\n";
    echo "<td><a href=\"timegraphdata_custom.php?ip=$val[0]&datefrom=$date&gran=min&direction=o\">$val[0]</a></td><td>$val[1]</td>\n";
    echo "</tr>\n";
  }
?>
</tbody>
</table>
</body>

<h1>HOUR</h1>

<body id="dt_example">

<table cellpadding="0" cellspacing="0" class="display" id="hour_in" border="1"> 
<thead>
<tr>
  <th>IP</th><th># Packets INBOUND</th>
</tr>
</thead>
<tbody>
<?php
  $json_object = exec('/usr/local/bin/firegazequery --dbprefix "sotm" --data -i -g hour --date ' . '"' . $date . '"' );
  $json_decoded = json_decode($json_object);

  foreach($json_decoded as $val) {
    echo "<tr class=\"ip\">\n";
    echo "<td><a href=\"timegraphdata_custom.php?ip=$val[0]&datefrom=$date&gran=hour&direction=i\">$val[0]</a></td><td>$val[1]</td>\n";
    echo "</tr>\n";
  }
?>
</tbody>
</table>

<table cellpadding="0" cellspacing="0" class="display" id="hour_out" border="1"> 
<thead>
<tr>
  <th>IP</th><th># Packets OUTBOUND</th>
</tr>
</thead>
<tbody>
<?php
  $json_object = exec('/usr/local/bin/firegazequery --dbprefix "sotm" --data -o -g hour --date ' . '"' . $date . '"' );
  $json_decoded = json_decode($json_object);

  foreach($json_decoded as $val) {
    echo "<tr class=\"ip\">\n";
    echo "<td><a href=\"timegraphdata_custom.php?ip=$val[0]&datefrom=$date&gran=hour&direction=o\">$val[0]</a></td><td>$val[1]</td>\n";
    echo "</tr>\n";
  }
?>
</tbody>
</table>
</body>

<h1>DAY</h1>

<body id="dt_example">

<table cellpadding="0" cellspacing="0" class="display" id="day_in" border="1"> 
<thead>
<tr>
  <th>IP</th><th># Packets INBOUND</th>
</tr>
</thead>
<tbody>

<?php
  $json_object = exec('/usr/local/bin/firegazequery --dbprefix "sotm" --data -i -g day --date ' . '"' . $date . '"' );
  $json_decoded = json_decode($json_object);

  foreach($json_decoded as $val) {
    echo "<tr class=\"ip\">\n";
    echo "<td><a href=\"timegraphdata_custom.php?ip=$val[0]&datefrom=$date&gran=day&direction=i\">$val[0]</a></td><td>$val[1]</td>\n";
    echo "</tr>\n";
  }
?>
</tbody>
</table>

<table cellpadding="0" cellspacing="0" class="display" id="day_out" border="1"> 
<thead>
<tr>
  <th>IP</th><th># Packets OUTBOUND</th>
</tr>
</thead>
<tbody>
<?php
  $json_object = exec('/usr/local/bin/firegazequery --dbprefix "sotm" --data -o -g day --date ' . '"' . $date . '"' );
  $json_decoded = json_decode($json_object);

  foreach($json_decoded as $val) {
    echo "<tr class=\"ip\">\n";
    echo "<td><a href=\"timegraphdata_custom.php?ip=$val[0]&datefrom=$date&gran=day&direction=o\">$val[0]</a></td><td>$val[1]</td>\n";
    echo "</tr>\n";
  }
?>
</tbody>
</table>
</body>

</head>
</html>
