<?php
function sanitize_input($var) {
  if (!($var == "day" || $var == "hour" || $var == "min" || $var == "i" || $var == "o" || preg_match('/^\d{1,3}\.?\d{0,3}\.?\d{0,3}\.?\d{0,3}$/', $var))) { // Granularity, direction, and IP
    if (!preg_match('/^\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{1,2}$/', $var)) {  // For date
      if (!preg_match('/^\d{10,13}$/', $var)) { // For Timestamp
        die("User input not accepted");
      }
    }
  } else {
    return;
  }
}
