<?php
/**
  * Extract log counts by time granularity
  *
  * @param start {Integer} The starting point in JS time
  * @param end {Integer} The ending point in JS time
  * @param ip {String} The IP represented as a regex string
  * @param direction {String} In or Out connection
  */

include 'sanitize.php';

date_default_timezone_set('UTC'); // So that strtotime converts to UTC

// get the parameters
if (isset($_GET['start'])) {
  sanitize_input($_GET['start']);
  $start = $_GET['start'];
} else {
  $start = (mktime() - (90 * 23 * 3600)) * 1000; // default span is 3 months
}

if (isset($_GET['end'])) {
  sanitize_input($_GET['end']);
  $end = $_GET['end'];
} else {
  $end = (mktime() + (48 * 3600)) * 1000; // two days ahead of current time, when not specified
}

if (isset($_GET['ip'])) {
  sanitize_input($_GET['ip']);
  $IP = $_GET['ip'];
} else {
  die('No IP specified');
}

if (isset($_GET['direction'])) {
  sanitize_input($_GET['direction']);
  $DIRECTION = $_GET['direction'];
} else {
  die('No Direction specified');
}

$range = $end - $start;

// half a day range loads minute data; 720 points
if ($range < 13 * 3600 * 1000) {
  // convert ts to string formatted date, minute granularity
  $DATE_START = gmdate("Y-m-d H:i", $start / 1000);
  $DATE_END = gmdate("Y-m-d H:i", $end / 1000);
  $interval = 60 * 1000;
  $gran = "min";

// two weeks range loads hourly data; 744 points
} elseif ($range < 14 * 24 * 3600 * 1000) {
  // convert ts to string formatted date, discard minute
  $DATE_START = gmdate("Y-m-d H:00", $start / 1000);
  $DATE_END = gmdate("Y-m-d H:00", $end / 1000);
  $interval = 3600 * 1000;
  $gran = "hour";

// one year loads daily data; whoe cares
// ($range < 15 * 31 * 24 * 3600 * 1000)
} else {
  $DATE_START = gmdate("Y-m-d 00:00", $start / 1000);
  $DATE_END = gmdate("Y-m-d 00:00", $end / 1000);
  // convert ts to string formatted date, discard hour and minute
  $interval = 24 * 3600 * 1000;
  $gran = "day";
}

// Get a list of all the IPs
$all_ips = exec('/usr/local/bin/firegazequery --listips -'.$DIRECTION.' --date "'.$DATE_START.'" --ips "^'.$IP.'"');
$results = array();
$rows = array();

// Define a starting date
$i = strtotime($DATE_START) * 1000;

foreach (json_decode($all_ips) as $ip) {
  // for each IP, get the values and push it onto the rows array
  //echo $ip;
  $r = exec("/usr/local/bin/firegazequery --histplot --data -$DIRECTION --hg \"$gran\" --hstart \"$DATE_START\" --hend \"$DATE_END\" --ips \"$ip$\""); // Terminate with $
  //echo "R is:";
  //print_r($r);
  foreach (json_decode($r) as $value) {
    $rows[] = "[$i, $value]";
    $i += $interval;
  }

  // join the rows array and push onto the results array
  $results[] = "[\"$ip\",[".join(", ",$rows)."]]";
  $i = strtotime($DATE_START) * 1000; // RESET counter
  $rows = array(); // RESET array
}

// join the results array
echo "[" . join(",", $results)."]";

?>
