<html>
<head>

<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script src=js/highcharts.js></script>
<script src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/stock/modules/exporting.js"></script>


<?php
  if (isset($_GET['ip'])) {
    $IP = $_GET['ip'];
    
    if (isset($_GET['gran']) && !empty($_GET['gran'])) {
      $GRAN = $_GET['gran'];
    } else {
      die('<h2>Specify acceptable granularity</h2>');
    }

    if (isset($_GET['direction']) && !empty($_GET['direction'])) {
      $DIRECTION = $_GET['direction']; // should be o or i
    } else {
      die('<h2>Specify acceptable direction</h2>');
    }

    if (isset($_GET['end']) && !empty($_GET['end'])) {
      $end = $_GET['end'];
    }

    if (isset($_GET['start']) && !empty($_GET['start'])) {
      $start = $_GET['start'];
    }


  } else {
    die('<h2>No IP specified</h2>');
  }

?>

<a href="all_ports_custom.php">Go to Dash</a>

<!-- This form calls this page with the given params, and IP regex -->
<form action="" method="get">
  IP regex: <input type="text" name="ip" value="<? echo $IP; ?>" />
  <input type="radio" name="direction" value="i" <?php if ($DIRECTION == "i") { echo 'checked'; } ?>/>In
  <input type="radio" name="direction" value="o" <?php if ($DIRECTION == "o") { echo 'checked'; } ?>/>Out
  <input type="hidden" name="gran" value="<?php echo $GRAN; ?>" />
  <input type="submit" value="Submit" />
</form>


<div id="container" style="height: 500px; min-width: 600px"></div>


<script>
$(function() {
    //get alert level 
    $.getJSON('from-mongo_ports_custom.php?ip=<?php echo $IP?>&direction=<?php echo $DIRECTION?>', function(data) { //TODO Use set extremes depending on granularity
        console.log(data.length);
        var resultSize = data.length;

        var seriesData = [];
    
        // for each IP matching the regex, create series data 
        for (var i = 0; i < resultSize; i++) {
          seriesData[i] = {
            type: "line",
            marker: {symbol: 'circle', radius: 1},
            name: data[i][0],
            data: data[i][1]
          }
        }

        // create the chart
        window.chart = new Highcharts.StockChart({
            chart : {
                renderTo : 'container',
                zoomType: 'x',
                events: {
                  click: function(e) {
                    // Alert line attributes
                    this.yAxis[0].removePlotLine('dangerline');
                    var y = e.yAxis[0].value;
                    this.yAxis[0].addPlotLine({
                      value: y,
                      color: 'red',
                      width: 2,
                      id: 'dangerline',
                      dashStyle: 'shortdash',
                      label: {
                        text: 'Alert level'
                      }
                    });
                   
                    // Set an alert threshold at this y value when clicked 
                    $.getJSON('setthresh.php', {val : y, gran : "<?php echo $GRAN; ?>", ip : "<?php echo $IP ?>", direction : "<?php echo $DIRECTION ?>"}, function(data) {
                      alert("Test affirmative: "+data.rval);
                    }).error(function() { alert("error"); });
                  }
                }
            },

            navigator : {
                //baseSeries: 0,
                adaptToUpdatedData: false,
                series : {
                    data : data[0][1] // TODO, check if this is necessary
                }
            },

            title: {
                text: 'Firewall Traffic <?php echo "$IP "; if ($DIRECTION == "i") { echo "INBOUND";} else { echo "OUTBOUND";} ?>'
            },
            
            subtitle: {
                text: 'Click and drag the plot area to zoom in'
            },
            
            rangeSelector : {
                buttons: [{
                    type: 'hour',
                    count: 12,
                    text: '12h'
                }, {
                    type: 'day',
                    count: 1,
                    text: '1d'
                }, {
                    type: 'week',
                    count: 1,
                    text: '1w'
                }, {
                    type: 'month',
                    count: 1,
                    text: '1m'
                }, {
                    type: 'all',
                    text: 'All'
                }],
                inputEnabled: false, // Do not show the dates
                selected : 4 // Select all by default 
            },
            
            xAxis : {
                events : {
                    afterSetExtremes : afterSetExtremes
                },
                plotLines: [{
                  id: "currenttime",
                  value: <?php echo (time())*1000;?>,
                  color: 'blue',
                  width: 2,
                  label : {
                    text: 'Current Time'
                  }
                }],
                minRange: 3600 * 1000 // one hour
            },

            yAxis: {
              title: {
                text: '# Packets'
              },
              maxPadding: 1.3 // Give a padding so that the alert level may be set
            },
            tooltip: {
              crosshairs: false, // We don't want ugly crosshairs which are neabled by default
              shared: false // This slows it down for multiple lines when enabled
            },

            plotOptions: {
              turboThreshold: 2000,
              line: {
                marker : {
                  symbol: 'circle',
                } 
              },
              series: {
                cursor: 'pointer',
                point: {
                  events: {
                    click: function() {
                      var gmttime = this.x/1000;
                      //var offset = new Date().getTimezoneOffset() * 60;
                      var offset = 0;
                      var localtime = gmttime + offset;
                      var range = chart.xAxis[0].getExtremes().max - chart.xAxis[0].getExtremes().min;

                      var minSize = 13 * 3600 * 1000; // for minute granularity
                      var hourSize = 14 * 24 * 3600 * 1000; // for hour granularity
                      var gr = "day";

                      if (range < minSize) {
                        gr = "min";
                      } else if (range < hourSize) {
                        gr = "hour";
                      }

                      window.location.pathname = 'firegaze/portbreakdown_custom.php?gran='+gr+'&ip='+this.series.name+'&direction=<?php echo $DIRECTION;?>&date='+localtime;
//                      hs.htmlExpand(null, {
//                        pageOrigin: {
//                          x: this.pageX,
//                          y: this.pageY
//                        },
//                        maincontentId: 'portbreakdown',
//                        width: 200
//                      });
                    }
                  }
                },
                marker: {
                  lineWidth: 0,
                  symbol: 'circle',
                  radius: 1,
                  states: { hover: { radius: 3 }} // This radius determines how big the clickable points are
                }
              }
            },

            series : seriesData
        }, function (chart) { // This function gives the navigator the series
            // We define the previous span in order to decide whether we want to redraw the chart when zooming
            window.previousSpan = chart.xAxis[0].getExtremes().max - chart.xAxis[0].getExtremes().min;
            console.log("PREVIOUS SPAN " + window.previousSpan);

            for (var i = 0; i < resultSize; i++) {
            chart.addSeries({
              enableMouseTracking: false,
              data: data[i][1],
              xAxis: chart.xAxis.length -1, // The navigator axis
              yAxis: chart.yAxis.length -1,
              color:chart.options.colors[i]})
              }

              console.log("adding series");
            });
    });
});


/**
 * Load new data depending on the selected min and max
 */
function afterSetExtremes(e) {

    var url,
        currentExtremes = this.getExtremes(),
        range = e.max - e.min;

    var minSize = 13 * 3600 * 1000; // for minute granularity
    var hourSize = 14 * 24 * 3600 * 1000; // for hour granularity

    console.log("previous: " + window.previousSpan);

    console.log("Current range: " + range);
    console.log("Previou range: " + window.previousSpan);

    // Don't redraw if we remain in the same span after zooming
    if (parseInt(window.previousSpan) <= minSize && range <= minSize) {
      window.previousSpan = range;
      return;
    } else if (parseInt(window.previousSpan) <= hourSize && range <= hourSize) {
      window.previousSpan = range;
      return;
    // If they are equal, we have reset the navigator to full view
    } else if (parseInt(window.previousSpan) > hourSize && range > hourSize && range < previousSpan) {
      console.log(hourSize);
      console.log(window.previousSpan);
      window.previousSpan = range;
      return;
    }

    // Load the data asynchronously
    chart.showLoading('Loading data from server...');
    $.getJSON('from-mongo_ports_custom.php?start='+ Math.round(e.min) +
            '&end='+ Math.round(e.max)+'&ip=<?php echo $IP; ?>'+'&direction=<?php echo $DIRECTION?>', function(data) { //TODO, check if we need to send more params

    var resultSize = data.length;
    var seriesData = [];

    for (var i = 0; i < resultSize; i++) {
      seriesData[i] = {
        name: data[i][0],
        data: data[i][1]
      }
    chart.series[i].setData(seriesData[i].data); // update the series data
    console.log(data[i][1].length);
    }

    console.log("Range: " + range);
    console.log("Start: " + new Date(Math.round(e.min)).toUTCString());
    console.log("End: " + new Date(Math.round(e.max)).toUTCString());

    chart.hideLoading();
    });
    
}

</script>

</head>
</html>
