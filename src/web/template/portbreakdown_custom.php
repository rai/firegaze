<html>
<head>

<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>

<style type="text/css">
  @import "js/demo_page.css";
  @import "js/demo_table.css";
a{ color: #330000; }
.ui-widget { font-size: 1em;}

#dt_example {
  margin: 0px auto;
  width: 600px
}
</style>

<script type="text/javascript" charset="utf-8">
  $(document).ready(function () {
    $('#portbreakdown').dataTable({
      "iDisplayLength": 10,
      "aaSorting": [[1, "desc"]] });
    $('#srcbreakdown').dataTable({
      "iDisplayLength": 10,
      "aaSorting": [[1, "desc"]] });
    });
</script>

</head>

<?php 

$PREFIX = "nim";

if (isset($_GET['date']) && !empty($_GET['date'])) {

  if (isset($_GET['ip']) && !empty($_GET['ip'])) {
    $IP = $_GET['ip'];
  }

  if (isset($_GET['direction']) && !empty($_GET['direction'])) {
    $DIRECTION = $_GET['direction'];
  }

  if (isset($_GET['gran']) && !empty($_GET['gran'])) {
    $GRAN = $_GET['gran'];
  }

  $DATE = date("Y-m-d H:i", $_GET['date']);
  #echo "/usr/local/bin/firegazequery --dbprefix $PREFIX --portbreakdown -g $GRAN -$DIRECTION --date \"$DATE\" --ips \"^$IP\"\n";
  $json_object = exec('/usr/local/bin/firegazequery --dbprefix '.$PREFIX.' --portbreakdown -g '.$GRAN.' -'.$DIRECTION.' --date "'.$DATE.'" --ips "^'.$IP.'"');
  $PORTS = json_decode($json_object);
  #echo $DATE.'<br />';
  #echo $IP.'<br />';
?>

<h1>Port Breakdown of Packets</h1>

<body id="dt_example">
<table cellpadding="0" cellspacing="0" class="display" id="portbreakdown" border="1">
<thead>
<tr>
  <th>Port</th><th># Packets</th>
</tr>
</thead>
<tbody>

<?php
  foreach ($PORTS as $subarray) {
    echo "<tr class=\"ports\"><td>$subarray[0]</td><td>$subarray[1]</td></tr>\n";
  }
?>
</tbody
</table>

<?php
$json_object = exec('/usr/local/bin/firegazequery --dbprefix '.$PREFIX.' --srcbreakdown -g '.$GRAN.' -'.$DIRECTION.' --date "'.$DATE.'" --ips "^'.$IP.'"');
$SRCS = json_decode($json_object);
?>

<table cellpadding="0" cellspacing="0" class="display" id="srcbreakdown" border="1">
<thead>
<tr>
  <th>Source</th><th># Packets</th>
</tr>
</thead>
<tbody>

<?php
  foreach ($SRCS as $subarray) {
    $subarray[0] = str_replace("-", ".", $subarray[0]);
    if ($DIRECTION == "i") {
      $SRC_DIR = "o";
    } else {
      $SRC_DIR = "i";
    }

    echo "<tr class=\"srcs\">\n";
    echo "<td><a href=\"highstockgraph_data_custom.php?ip=$subarray[0]&datefrom=$DATE&gran=$GRAN&direction=$SRC_DIR\">$subarray[0]</a></td><td>$subarray[1]</td>\n";
    echo "</tr>\n";
  }
?>

</tbody
</table>
</body>
<?php
} else {
  die();
}
?>

<h1>Source IP Breakdown of Packets</h1>

</html>
