#!/bin/sh -e

set -e

if [ $# -eq 0 ]; then
  echo "Usage: "
  echo "firegaze init"
  echo "firegaze start"
  echo "firegaze stop"
  exit 1
fi

COMMAND="$1"
CONFDIR="/etc/firegaze/firegaze.conf"
FIREWALL_LOCK=/var/lock/firewall_iptables.lock
IPTABLES="/sbin/iptables"
#RSYSLOGD="/usr/sbin/rsyslogd -c3"
CAPTURELOG="/tmp/fgazecap.log"
shift

makedirs() {
  WEBDIR=`awk '/web_dir/ {print $3}' $CONFDIR`
  GRAPHDIR=`awk '/graph_dir/ {print $3}' $CONFDIR`
  PIPEDIR=`awk '/pipe_dir/ {print $3}' $CONFDIR`
  RRDDIR=`awk '/rrd_dir/ {print $3}' $CONFDIR`
  if [ -z "$WEBDIR" ] || [ -z $GRAPHDIR ] || [ -z $PIPEDIR ] || [ -z $RRDDIR ]; then
    echo "Failed! Check that $WEBDIR, $GRAPHDIR, $PIPEDIR, AND $RRDDIR are specified in $CONFDIR"
    exit 1
  fi 

  if [ ! -d $WEBDIR ]; then
    echo "Creating web directory $WEBDIR"
    mkdir -p $WEBDIR  
    chmod 755 $WEBDIR
  fi

  if [ ! -d $GRAPHDIR ]; then
    echo "Creating graph directory $GRAPHDIR"
    mkdir $GRAPHDIR 
    chmod 755 $GRAPHDIR
  fi

  if [ ! -p $PIPEDIR ]; then
    echo "Creating Log Pipe $PIPEDIR"
    mkfifo $PIPEDIR
  fi

  if [ ! -d $RRDDIR ]; then
    echo "Creating RRD directory $RRDDIR"
    mkdir $RRDDIR
    chmod 755 $RRDDIR
  fi
}

configiptables() {
  echo "Adding iptables rules"

  set +e #Don't exit on error, if chain exists

  $IPTABLES -N from_eth0
  $IPTABLES -N to_eth0
  $IPTABLES -I INPUT -j from_eth0
  $IPTABLES -I OUTPUT -j to_eth0
  $IPTABLES -A from_eth0 -i eth0 -j LOG --log-prefix "FWLOG=IN "
  $IPTABLES -A to_eth0 -o eth0 -j LOG --log-prefix "FWLOG=OUT "
  
  set -e
}

restartrsyslog() {
  echo "Killing rsyslog Daemon"
  pkill -9 rsyslogd # /usr/bin/pkill
  echo "Starting rsyslog Daemon"
  /usr/sbin/rsyslogd -c3
}

killfiregazecap() {
  FIREPID=`ps aux | awk '/bin\/firegazecapture/ {print $2}'`

  if [ ! -z "$FIREPID" ]; then 
    echo "Killing Log Writer @PID $FIREPID"
    kill $FIREPID # /usr/bin/kill or /usr/kill
    echo "Log Writer stopped"
    return
  fi

  echo "Log Writer already stopped"
}

startfiregazecap() {
  FIREPID=`ps aux | awk '/bin\/firegazecapture/ {print $2}'`

  if [ -z "$FIREPID" ]; then 
    echo "Starting Log Writer"
    echo "Logging to $CAPTURELOG"
    nohup /usr/local/bin/firegazecapture 2> $CAPTURELOG &
    return
  fi

  echo "Already running @$FIREPID"
}

case $COMMAND in
status)
  echo "Status message"
  ;;
init)
  echo "Initializing"

  makedirs
  configiptables  

  echo
  echo "Done"
  exit 0
  ;;
start)
  echo "Starting"

  restartrsyslog
  sleep 5
  startfiregazecap

  echo
  echo "Done"
  exit 0
  ;;
stop)
  echo "Stopping"
  killfiregazecap
  echo
  echo "Done"
  exit 0
  ;;
restart)
  echo "Use start/stop"
  ;;
*)
  echo "no option"
  exit 1
esac
