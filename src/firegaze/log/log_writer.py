'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
       
Created on 13 Jul 2012

@author: Rijnard
'''

import time as t
from datetime import datetime, time
import sys
                
class LogWriter(object):
    '''
    A listener for iptables log messages.
    '''

    def __init__(self, logpipe, dbhandler):
        '''
        Constructor for the log writer
        '''    
        self.logpipe = logpipe  # Log pipes to this file, listener reads from it
        self.dbhandler = dbhandler
        
    def make_document(self, ip_field, port_field, logdoc):
        '''
        Construct the document that will be inserted or updated
        in the database
        '''
        
        if not logdoc:
            sys.stderr.write('empty logdoc')
            return None
        
        try:
            log_date = datetime.utcnow()
            
            if (logdoc['PROTO'] == 'TCP' or logdoc['PROTO'] == 'UDP'):
                try:
                    port = int(logdoc[port_field])
                except ValueError:
                    port = -2  # A different protocol, no port number
            else:
                port = -1  # Assign this for protocols such as ICMP
                
            # Get the day of the date    
            day_date = datetime.combine(log_date.date(), time.min) 
            # Efficient support for range queries for several months
            id_daily = logdoc[ip_field] + log_date.strftime('/%Y%m%d') 
            hour = log_date.hour
            minute = log_date.minute
                    
            daily_query = {'_id': id_daily,
                           'date': day_date,
                           'IP': logdoc[ip_field]
                           }
            
            src_addr = logdoc['SRC']
            src_addr = src_addr.replace(".", "-")  # no dot notation allowed in mongoDB
                    
            daily_update = { '$inc': {'hourly.%d.%s.%d' % (hour, 'ports', port): 1,
                                      'hourly.%d.%s.%s' % (hour, 'ports', 'total') : 1,
                                      'hourly.%d.%s.%s' % (hour, 'srcs', src_addr) : 1,
                                      'minute.%d.%d.%s.%d' % (hour, minute, 'ports', port): 1,
                                      'minute.%d.%d.%s.%s' % (hour, minute, 'ports', 'total') : 1,
                                      'minute.%d.%d.%s.%s' % (hour, minute, 'srcs', src_addr) : 1 }}
            
            id_monthly = logdoc[ip_field] + log_date.strftime('/%Y%m')
            day_of_month = log_date.day
                    
            monthly_query = {'_id': id_monthly,
                             'date': day_date.replace(day=1),
                             'IP': logdoc[ip_field]
                             }
            
            monthly_update = {'$inc' : {'daily.%d.%s.%d' % (day_of_month, 'ports', port): 1,
                                        'daily.%d.%s.%s' % (day_of_month, 'ports', 'total') : 1,
                                        'daily.%d.%s.%s' % (day_of_month, 'srcs', src_addr) : 1}}
            
        except KeyError as err:
            sys.stderr.write("Unknown key not found " + err.__str__())
            return None
        
        return (daily_query, daily_update, monthly_query, monthly_update)
        
        
    def listen(self):
        '''
        Start the listener. It logs the source address and source port by default.
        '''
        
        print 'Starting listener'
        logfile = open(self.logpipe, 'r')
        
        while True:
            log_line = logfile.readline()
            if log_line:
                # get a dictionary of the log
                logdoc = _log_to_dict(log_line) 
                try:
                    if logdoc['FWLOG'] == 'IN':  # If it is an incoming packet
                        ip_field = 'DST'
                        port_field = 'DPT'
                        doc = self.make_document(ip_field, port_field, logdoc)

                        if doc:
                            self.dbhandler.daily_in_collection.update(doc[0],
                                                                  doc[1],
                                                                  upsert=True)
                            self.dbhandler.monthly_in_collection.update(doc[2],
                                                                    doc[3],
                                                                    upsert=True)
                    elif logdoc['FWLOG'] == 'OUT':  # If it is an outgoing packet
                        ip_field = 'DST'
                        port_field = 'DPT'
                        doc = self.make_document(ip_field, port_field, logdoc)
                        if doc:
                            self.dbhandler.daily_out_collection.update(doc[0],
                                                                   doc[1],
                                                                   upsert=True)
                            self.dbhandler.monthly_out_collection.update(doc[2],
                                                                     doc[3],
                                                                     upsert=True)
                except KeyError:
                    sys.stderr.write("Invalid document, cannot insert " + 
                                     log_line + ' (' + logdoc.__str__() + ')')
            else:
                t.sleep(1)
                
def _log_to_dict(log):
    '''
    Parses a log line to a dictionary
    '''
    dic = {}
    for item in log.split():
        try:
            key, val = item.split('=')
            dic[key] = val
        except ValueError:  # if there is no value
            pass
    return dic 
