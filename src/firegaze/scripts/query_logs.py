'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
       
Created on 01 Sep 2012

@author: Rijnard
'''
# pylint: disable-msg=C0103

import sys, os, json
from optparse import OptionParser
sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from firegaze.util.query import FWLogQuery, get_json_data_points
from datetime import datetime

def _fix_date(timestamp):
    '''
    Converts a date string to a datetime object
    '''
    try:
        date = datetime.strptime(timestamp, "%Y-%m-%d %H:%M")
    except ValueError:
        try:
            date = datetime.strptime(timestamp, "%Y-%m-d %H")
        except ValueError:
            date = datetime.strptime(timestamp, "%Y-%m-%d")
        
    return date

def main():
    '''
    Command line parser for constructing queries and retrieving JSON output
    '''
    
    # FOR DATATABLES
    parser = OptionParser()
    parser.add_option("--data", action="store_true", dest="data", help="Stats for data")
    parser.add_option("--port", action="store_true", dest="port", help="Stats for port")
    parser.add_option("--date", action="store", dest="date", default="2012-01-01 00:00", help="Stats for this date")
    parser.add_option("-i", action="store_true", help="Stats for incoming traffic")
    parser.add_option("-o", action="store_true", help="Stats for outgoing traffic")
    parser.add_option("-g", action="store", default="day", help="Stats for granularity")
    parser.add_option("--dbprefix", action="store", default='', help="Specify a custom database by prefix")
    # FOR HISTORIC PLOTS
    parser.add_option("--histplot", action="store_true", help="Historic plot")
    parser.add_option("--hstart", action="store", default="2012-01-01 00:00", help="Start time for historic plot") #end-2h
    parser.add_option("--hend", action="store", default="2012-01-01 00:00", help="End time for historic plot")
    parser.add_option("--hg", action="store", default="day", help="Granularity for historic plot") 
    parser.add_option("--ips", action="store_true", default="127.0.0.1", help="List of IPs to plot")
    # FOR PORT BREAKDOWN
    parser.add_option("--portbreakdown", action="store_true", help="Data packet breakdown by port")
    # FOR GETTING LIST OF IPS FROM REGEX
    parser.add_option("--listips", action="store_true", help="List IPs matching the expression")
    #FOR SRC BREAKDOWN
    parser.add_option("--srcbreakdown", action="store_true", help="Data packet breakdown by src")

#    options, args = parser.parse_args(['--histplot', '-o', '--hg', 'day', '--hstart', '2012-08-13 23:00', '--hend', '2012-09-30 23:25', '--ips', '^66.220.151.77'])
#    options, args = parser.parse_args(['--portbreakdown', '-g', '"day"', '-o', '--date', '2012-09-15 11:29', '--ips', '^66.220.151.77'])
#    options, args = parser.parse_args(['--portbreakdown', '-g', 'day', '-o', '--date', '2012-09-25 11:11', '--ips', '^92.122.208.57'])
#    options, args = parser.parse_args(['--listips', '-o', '--date', '2012-09-25 11:11', '--ips', '^92.122.208.9'])
    options, args = parser.parse_args()
    
    date = _fix_date(options.date)
    start_date = _fix_date(options.hstart)
    end_date = _fix_date(options.hend)
    
    if options.listips:
#        args = ['^146.232.20 ^10.0.2.2']
        args = args[0].split()
        print 'args', args
        query = FWLogQuery(options.o, options.dbprefix)
        labels = query.get_ips(args)
        print(json.JSONEncoder().encode(labels))
        sys.exit(0)
        
    if options.srcbreakdown:
        query = FWLogQuery(options.o, options.dbprefix)
        labels = query.get_ips(args)
        if len(labels) == 0:
            sys.stderr.write("No valid IP")
            sys.exit(1)
            
        if options.g == 'day':
            query = FWLogQuery(options.o, options.dbprefix).get_day_packet_counts_by_srcs
        elif options.g == 'hour':
            query = FWLogQuery(options.o, options.dbprefix).get_hour_packet_counts_by_srcs
        elif options.g == 'min':
            query = FWLogQuery(options.o, options.dbprefix).get_minute_packet_counts_by_srcs
        else:
            raise Exception()
        
        ip = labels[0]
        packet_by_srcs = query(ip, _fix_date(options.date))
        
        print(json.JSONEncoder().encode(packet_by_srcs))
        sys.exit(0)
    
    if options.portbreakdown:
        
        query = FWLogQuery(options.o, options.dbprefix)
        print args
        labels = query.get_ips(args) # may only have len of 1
        if len(labels) == 0:
            sys.stderr.write("No valid IP")
            sys.exit(1)
            
        if options.g == 'day':
            query = FWLogQuery(options.o, options.dbprefix).get_day_packet_counts_by_port
        elif options.g == 'hour':
            query = FWLogQuery(options.o, options.dbprefix).get_hour_packet_counts_by_port
        elif options.g == 'min':
            query = FWLogQuery(options.o, options.dbprefix).get_minute_packet_counts_by_port
        else:
            raise Exception()
    
        ip = labels[0]
        packet_by_port = query(ip, _fix_date(options.date))

        print(json.JSONEncoder().encode(packet_by_port))
        sys.exit(0)
    
    if options.histplot:
        labels = args
        labels = FWLogQuery(options.o, options.dbprefix).get_ips(args)
#        print labels
        if len(labels) == 0:
            sys.stderr.write("No valid IP")
            sys.exit(1)
            
        if options.data:
            if options.hg == 'day': # good for many days
                query = FWLogQuery(options.o, options.dbprefix).get_day_packet_counts
                res = 24 * 3600
            elif options.hg == 'hour': # good for a week or so
                query = FWLogQuery(options.o, options.dbprefix).get_hour_packet_counts
                res = 3600
            elif options.hg == 'min': # good for a day or two
                query = FWLogQuery(options.o, options.dbprefix).get_minute_packet_counts
                res = 60
        elif options.port:
            if options.hg == 'day': # good for many days
                query = FWLogQuery(options.o, options.dbprefix).get_day_port_counts
                res = 24 * 3600
            elif options.hg == 'hour': # good for a week or so
                query = FWLogQuery(options.o, options.dbprefix).get_hour_port_counts
                res = 3600
            elif options.hg == 'min': # good for a day or two
                query = FWLogQuery(options.o, options.dbprefix).get_minute_port_counts
                res = 60
        else:
            sys.stderr.write("Error, specify data or port option")
            sys.exit(1)

        counts = get_json_data_points(start_date, 
                                      end_date, 
                                      query, 
                                      res, 
                                      labels)
        print(json.JSONEncoder().encode(counts))
        sys.exit(0) # Done if this is a history plot
    
    if options.data or options.port:
        if options.data:
            if options.g == 'min':
                query = FWLogQuery(options.o, options.dbprefix).highest_minute_packet_counts
            elif options.g == 'hour':
                query = FWLogQuery(options.o, options.dbprefix).highest_hour_packet_counts
            elif options.g == 'day':
                query = FWLogQuery(options.o, options.dbprefix).highest_day_packet_counts
        elif options.port:
            if options.g == 'min':
                query = FWLogQuery(options.o, options.dbprefix).highest_minute_port_counts
            elif options.g == 'hour':
                query = FWLogQuery(options.o, options.dbprefix).highest_hour_port_counts
            elif options.g == 'day':
                query = FWLogQuery(options.o, options.dbprefix).highest_day_port_counts
        else:
            sys.stderr.write("Error")
            sys.exit(1)
                
        labels, counts = query(date)
        print(json.JSONEncoder().encode(zip(labels, counts)))
        sys.exit(0)
    else:
        raise Exception("Unknown Option")
    
if __name__ == '__main__':
    main()
