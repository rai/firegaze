'''
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
       
Created on 09 Jul 2012

@author: Rijnard
'''
# pylint: disable-msg=C0103

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from firegaze.log.log_writer import LogWriter
from firegaze.util.dbhandler import DBHandler
from firegaze.util.config import conf

def main(): # TODO specify port in conf
    '''
    Starts log capture
    '''
    
    dbhandler = DBHandler(conf.DAILY_DB_NAME,
                          conf.MONTHLY_DB_NAME) # MongoDB collection
    writer = LogWriter(conf.PIPE_DIR, dbhandler)
    writer.listen()

if __name__ == '__main__':
    main()

    
