'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
       
Created on 09 Oct 2012

@author: Rijnard
'''

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from datetime import datetime, time
from firegaze.log.log_writer import _log_to_dict
from firegaze.util.dbhandler import DBHandler
import time as t

def make_document(ip_field, port_field, logdoc, dt_obj):
    '''
    Construct the document that will be inserted or updated
    in the database
    '''
    
    if not logdoc:
        sys.stderr.write('empty logdoc')
        return None
    
    try:
        log_date = dt_obj
        
        try:
            if (logdoc['PROTO'] == ('TCP' or 'UDP')):
                port = int(logdoc[port_field])
            else: # TODO find specific numbers for protocols
                port = -1 # Assign this for protocols such as ICMP
        except KeyError as err:
            sys.stderr.write('No PROTO field '+err.__str__())
            port = -2 # If there is no proto field, assign it to -2
            
        # Get the day of the date    
        day_date = datetime.combine(log_date.date(), time.min) 
        # Efficient support for range queries for several months
        id_daily = logdoc[ip_field] + log_date.strftime('/%Y%m%d') 
        hour = log_date.hour
        minute = log_date.minute
                
        daily_query = {'_id': id_daily, 
                       'date': day_date,
                       'IP': logdoc[ip_field]
                       }
        
        #TODO add dropped packet functionality, SRC and SPT
        src_addr = logdoc['SRC']
        src_addr = src_addr.replace(".", "-") # no dot notation allowed with mongodb
                
        daily_update = { '$inc': {'hourly.%d.%s.%d' % (hour, 'ports', port): 1,
                                  'hourly.%d.%s.%s' % (hour, 'ports', 'total') : 1,
                                  'hourly.%d.%s.%s' % (hour, 'srcs', src_addr) : 1,
                                  'minute.%d.%d.%s.%d' % (hour, minute, 'ports', port): 1, 
                                  'minute.%d.%d.%s.%s' % (hour, minute, 'ports', 'total') : 1, 
                                  'minute.%d.%d.%s.%s' % (hour, minute, 'srcs', src_addr) : 1 }}
        
        id_monthly = logdoc[ip_field] + log_date.strftime('/%Y%m')
        day_of_month = log_date.day
                
        monthly_query = {'_id': id_monthly,
                         'date': day_date.replace(day=1),
                         'IP': logdoc[ip_field]
                         }
        
        monthly_update = {'$inc' : {'daily.%d.%s.%d' % (day_of_month, 'ports', port): 1, 
                                    'daily.%d.%s.%s' % (day_of_month, 'ports', 'total') : 1,
                                    'daily.%d.%s.%s' % (day_of_month, 'srcs', src_addr) : 1}}
        
    except KeyError as err:
        sys.stderr.write("Unknown key not found "+err.__str__())
        sys.stderr.write("Full logdoc: " + logdoc.__str__())
        return None
    
    return (daily_query, daily_update, monthly_query, monthly_update)

if __name__ == '__main__':
    '''
    This script is used to popoulate a new database with firewall logs
    residing on the file system.
    '''
    
    fi = open("/media/shared/sotm/nimbula/clust.log", "r+")
    DB_PREFIX = "nimbula"
    DEFAULT_DB_NAMES = ("dailylogs", "monthlylogs")
    
    dbhandler = DBHandler(DB_PREFIX + DEFAULT_DB_NAMES[0],
                          DB_PREFIX + DEFAULT_DB_NAMES[1])
    
    dbhandler.clear_collections()
    
    for n, line in enumerate(fi.readlines()):
        print n
        temp = line.split()
        date_str = "2012 " + ' '.join(temp[:3])
        rest = ' '.join(temp[3:])

        dt_obj = datetime.strptime(date_str, "%Y %b %d %H:%M:%S")
        logdoc = _log_to_dict(rest)

        if logdoc:
            try:
                if "FWLOG=IN" in rest:
                    doc = make_document("DST", "DPT", logdoc, dt_obj)
                    if doc:
                        dbhandler.daily_in_collection.update(doc[0], 
                                                              doc[1], 
                                                              upsert=True)
                        dbhandler.monthly_in_collection.update(doc[2], 
                                                                doc[3], 
                                                                upsert=True)
                else:
                    doc = make_document("DST", "DPT", logdoc, dt_obj)
                    if doc:
                        dbhandler.daily_out_collection.update(doc[0], 
                                                              doc[1], 
                                                              upsert=True)
                        dbhandler.monthly_out_collection.update(doc[2], 
                                                                doc[3], 
                                                                upsert=True)
            except KeyError:
                sys.stderr.write("No FWLOG field in log, ignoring "+
                                 line+'__('+logdoc.__str__()+')')
            
print "done"
                