'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
       
Created on 10 Jul 2012

@author: Rijnard
'''
# pylint: disable-msg=W0141, R0913, C0301

from pyrrd.rrd import DataSource, RRA, RRD
from pyrrd.graph import DEF, LINE, ColorAttributes, Graph, AREA
from datetime import datetime, timedelta, time
from firegaze.util.query import FWLogQuery, dateToTs
import time as t
import threading

def get_json_data_points(start_date, end_date, query, res, ips):
    '''
    Returns a list of data points between the start and end date for the given query
    '''

    date_to_ts = dateToTs(end_date)
    date_from_ts = dateToTs(start_date)
    
    second_span = date_to_ts - date_from_ts
    span = second_span/res
    
    num = len(ips)
    data_pts = []
    
    for val in xrange(span):
        plot_date = start_date + (timedelta(0, res)*val)
        counts = query(ips[:num], plot_date)
        data_pts.append(counts[0])
    
    return data_pts

class Plotter(threading.Thread):
    '''
    Plots firewall metrics
    '''

    def __init__(self, labels, start_time, heart_beat, the_step, rrd_file, graph_output):
        '''
        Constructs the RRD with appropriate Data Sources and labels
        '''
        self.graph_output = graph_output
        self.filename = rrd_file
        self.data_sources = []
        self.labels = labels
        round_robin_archives = []
        for i in xrange(len(self.labels)):
            self.data_sources.append(DataSource(dsName='count'+str(i), 
                                                dsType='GAUGE', 
                                                heartbeat=heart_beat))
        
        # One PDP is consolidated on a per hour basis over one week
        round_robin_archives.append(RRA(cf='AVERAGE', 
                                        xff=0.5, 
                                        steps=1, 
                                        rows=70*24*60)) 
        #round_robin_archives.append(RRA(cf='LAST', xff=0.5, steps=1, rows=24))
        self.my_rrd = RRD(self.filename, 
                          ds=self.data_sources, 
                          rra=round_robin_archives, 
                          start=start_time, 
                          step=the_step)

        self.my_rrd.create()
            
        threading.Thread.__init__(self)
        
    def add_datapoints(self, ttime, *args):
        '''
        Adds the data points supplied by *args to the rrd
        '''
        self.my_rrd.bufferValue(ttime, *args)
        self.my_rrd.update()
        
    def graph(self, start_time, end_time, vlabel, my_title=''):
        '''
        Graphs the data of this rrd from start_time to end_time
        '''
        
        defs = []
        lines = []
        colors = ['#FFFFFF', 
                  '#00CCFF',
                  '#0066FF',
                  '#0033FF',
                  '#9933FF', #Light purple
                  '#FF66CC', #Dark purple
                  '#CC3300', #Dull red
                  '#FF6600', #Dark orange
                  '#FF9900', #Light orange
                  '#FFFF33', #Yellow
                  '#99FF33', #Light green
                  '#009900', #Dark green
    #              '#FF3333'
                  ]
        
        t_colors = map(lambda x: x+'11', colors)
        
        for i in xrange(len(self.labels)):
            defs.append(DEF(rrdfile=self.my_rrd.filename, 
                            vname=str(i), dsName='count'+str(i)))
            
            lines.append(AREA(defObj=defs[i], 
                              color=t_colors[i % len(t_colors)], 
                              ))
            
            lines.append(LINE(defObj=defs[i], 
                      color=colors[i % len(colors)], width=1,
                      legend=self.labels[i]))
        
        att = ColorAttributes()
        att.back = '#333333'
        att.canvas = '#333333'
        att.shadea = '#000000'
        att.shadeb = '#111111'
        att.mgrid = '#CCCCCC'
        att.axis = '#FFFFFF'
        att.frame = '#AAAAAA'
        att.font = '#FFFFFF'
        att.arrow = '#FFFFFF'
        
        graph = Graph(self.graph_output, imgformat='PNG', 
                  width=640, height=480, 
                  start=start_time, end=end_time, 
                  vertical_label=vlabel, color=att, title=my_title)
        graph.data.extend(defs+lines)
        graph.write()
        
    def run(self):
        pass
    
class MinutePlot(Plotter):
    '''
    Plots for Minute granularity
    '''
    def __init__(self, labels, rrd_file, graph_output):
        
        # two minutes before specified start
        rrd_start = dateToTs(datetime.utcnow() - timedelta(0, 120, 0))
        
        self.labels = labels
        
        print 'start', rrd_start
        super(MinutePlot, self).__init__(labels, rrd_start, 
                                         120, 60, rrd_file, graph_output)
        self.query = FWLogQuery()
        
    def update(self):
        pass
        
    def run(self):
        while True:
            print 'Updating rrd'
            self.update()
            sleep_time = 60 - datetime.utcnow().second
            print 'Sleeping for ', sleep_time
            t.sleep(sleep_time)

class MinuteDataPlot(MinutePlot):
    '''
    Plots Data for Minute granularity
    '''
    def __init__(self, labels, rrd_file, graph_output, span='-1h', title='', port='total'):
        
        self.title = title
        self.port = port 
        self.span = span        
        self.labels = labels
        
        super(MinuteDataPlot, self).__init__(labels, rrd_file, graph_output)
        self.query = FWLogQuery(out=True)
        
    def update(self):
        # plot the previous minute at rollover
        now_date = datetime.utcnow() - timedelta(0, 0, 0, 0, 1, 0, 0) 
            
        counts = self.query.get_minute_packet_counts(self.labels,
                                                     now_date,
                                                     port=self.port)

        # align the insert date to the nearest minute,
        # otherwise RRD will not display correctly
        insert_date = datetime(now_date.year, now_date.month,
                               now_date.day, now_date.hour,
                               now_date.minute)

        print 'Adding', insert_date, counts
        self.add_datapoints(dateToTs(insert_date), *tuple(counts))
        
#        plot_historic(self.labels)

        self.graph(self.span, 'now', 'num_packets', self.title)

class MinutePortCountPlot(MinutePlot):
    '''
    Plots port counts for Minute granularity
    '''
    def __init__(self, labels, rrd_file, graph_output, span='-1h', title='', port='total'):
        
        self.title = title
        self.port = port 
        self.span = span
        self.labels = labels

        super(MinutePortCountPlot, self).__init__(labels,
                                                  rrd_file,
                                                  graph_output)
        self.query = FWLogQuery()
        
    def update(self):
        # plot the previous minute at rollover
        now_date = datetime.utcnow() - timedelta(0, 0, 0, 0, 1, 0, 0)
            
        counts = self.query.get_minute_port_counts(self.labels, now_date)

        # align the insert date to the nearest minute,
        # otherwise RRD will not display correctly
        insert_date = datetime(now_date.year, now_date.month,
                               now_date.day, now_date.hour,
                               now_date.minute)

        print 'Adding', insert_date, counts
        self.add_datapoints(dateToTs(insert_date), *tuple(counts))

        self.graph(self.span, 'now', 'num_ports', self.title)

class MinuteDataByPortPlot(MinutePlot):
    '''
    Plots port counts by most packets for Minute granularity
    '''
    def __init__(self, ip, labels, rrd_file, graph_output, span='-1h', title=''):
        self.ip = ip
        self.query = FWLogQuery()
        self.span = span
        self.title = ip if not title else title
        super(MinuteDataByPortPlot, self).__init__(labels,
                                                   rrd_file,
                                                   graph_output)
        
    def update(self, port_counts):
        now_date = datetime.utcnow() - timedelta(0, 0, 0, 0, 1, 0, 0)

        insert_date = datetime(now_date.year, now_date.month,
                               now_date.day, now_date.hour,
                               now_date.minute)
        
        print port_counts
        
        # predictably append port values
        counts = []
        for port in self.labels:
            try: # if the port exists, append it
                counts.append(port_counts[port])
            except KeyError: # if not, append 0
                counts.append(0)
        
        print 'Adding', insert_date, counts
        self.add_datapoints(dateToTs(insert_date), *tuple(counts))

        self.graph(self.span, 'now', 'num_packets', self.title)

class HourPlot(Plotter): #XXX Not implemented
    '''
    Incomplete RRD implementation for Hour granularity
    '''
    def __init__(self, ips, days=0, hours=0, minutes=0, weeks=0):
        start = dateToTs(datetime.utcnow() - timedelta(days,
                                                     60, 0, 0,
                                                     minutes,
                                                     hours,
                                                     weeks))
        
        super(HourPlot, self).__init__(ips, start, 7200, 3600)
    
class DayPlot(Plotter): #XXX Not implemented
    '''
    Incomplete RRD implementation for Day granularity
    '''
    def __init__(self, ips, days=0, hours=0, minutes=0, weeks=0):
        start = dateToTs(datetime.utcnow() - timedelta(days,
                                                     60, 0, 0,
                                                     minutes,
                                                     hours,
                                                     weeks))
        
        super(DayPlot, self).__init__(ips, start, 172800, 86400)
    
class MinuteDataPlotDual(MinutePlot):
    '''
    Experimental dual plot for Minute granularity
    '''
    def __init__(self, labels_in, labels_out, rrd_file, graph_output, span='-1h', title='', port='total'):
        
        self.title = title
        self.port = port 
        self.span = span        
        self.labels_in = labels_in
        self.labels_out = labels_out
        
        super(MinuteDataPlotDual, self).__init__(labels_out + labels_in, rrd_file, graph_output)
        self.query_out = FWLogQuery(out=True)
        self.query_in = FWLogQuery()
        
    def update(self):
        # plot the previous minute at rollover
        now_date = datetime.utcnow() - timedelta(0, 0, 0, 0, 1, 0, 0) 
            
        counts1 = self.query_out.get_minute_packet_counts(self.labels_out,
                                                          now_date,
                                                          port=self.port)
        counts2 = self.query_in.get_minute_packet_counts(self.labels_in,
                                                         now_date,
                                                         port=self.port)
        counts2 = map(lambda x: -x, counts2) #make negative

        # align the insert date to the nearest minute,
        # otherwise RRD will not display correctly
        insert_date = datetime(now_date.year, now_date.month,
                               now_date.day, now_date.hour,
                               now_date.minute)

        self.add_datapoints(dateToTs(insert_date), *tuple(counts1 + counts2))
        
#        plot_historic(self.labels)

        self.graph(self.span, 'now', '\<===In========Out===\>', self.title)

def graph_from_existing_rrd(start_time, end_time, vlabel, rrd_file, graph_out, labels):
    '''
    Graphs the data of this rrd from start_time to end_time
    '''
    
    defs = []
    lines = []
    colors = ['#FFFFFF', 
              '#00CCFF',
              '#0066FF',
              '#0033FF',
              '#9933FF', #Light purple
              '#FF66CC', #Dark purple
              '#CC3300', #Dull red
              '#FF6600', #Dark orange
              '#FF9900', #Light orange
              '#FFFF33', #Yellow
              '#99FF33', #Light green
              '#009900', #Dark green
              ]
    
    t_colors = map(lambda x: x+'11', colors)
    
    for i in xrange(len(labels)):
        defs.append(DEF(rrdfile=rrd_file, 
                        vname=str(i), dsName='count'+str(i)))
        
        lines.append(AREA(defObj=defs[i], 
                          color=t_colors[i % len(t_colors)], 
                          ))
        
        lines.append(LINE(defObj=defs[i], 
                  color=colors[i % len(colors)], width=1,
                  legend=labels[i]))
    
    att = ColorAttributes()
    att.back = '#333333'
    att.canvas = '#333333'
    att.shadea = '#000000'
    att.shadeb = '#111111'
    att.mgrid = '#CCCCCC'
    att.axis = '#FFFFFF'
    att.frame = '#AAAAAA'
    att.font = '#FFFFFF'
    att.arrow = '#FFFFFF'
    
    graph = Graph(graph_out, imgformat='PNG', 
              width=640, height=480, 
              start=start_time, end=end_time,
              vertical_label=vlabel, color=att)
    graph.data.extend(defs+lines)
    graph.write()