'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
       
Created on 09 Jul 2012

@author: Rijnard
'''
# pylint: disable-msg=W0104

from pymongo import Connection

class DBHandler(object):
    '''
    Handles connections to MongoDB
    '''

    def __init__(self, daily_collection, monthly_collection):
        '''
        Constructs a handler for the logs collections in the database
        '''
        self.daily_collection_name = daily_collection
        self.monthly_collection_name = monthly_collection
        
        self.db = Connection().logs # db name is logs
        self.db.slave_okay # Allow writing to slaves
        # collection name is the collection argument
        self.daily_in_collection = self.db[daily_collection+'_in']
        self.monthly_in_collection = self.db[monthly_collection+'_in']
        self.daily_out_collection = self.db[daily_collection+'_out']
        self.monthly_out_collection = self.db[monthly_collection+'_out']
        
    def clear_collections(self):
        '''
        Remove all log entries
        '''
        
        try:
            self.daily_in_collection.remove()
            self.monthly_in_collection.remove()
            self.daily_out_collection.remove()
            self.monthly_out_collection.remove()
        except:
            print "Error removing collections"
    
    def drop_collections(self):
        '''
        Drops the collections
        '''
        
        try:
            self.db.drop_collection(self.daily_collection_name+'_in')
            self.db.drop_collection(self.monthly_collection_name+'_in')
            self.db.drop_collection(self.daily_collection_name+'_out')
            self.db.drop_collection(self.monthly_collection_name+'_out')
        except:
            print "Error removing entries"