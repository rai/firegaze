'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
       
Created on 23 Aug 2012

@author: Rijnard
'''
# pylint: disable-msg=W0142, W0404, C0103

from dbhandler import DBHandler
from datetime import datetime, timedelta, time
import operator
import time as t
import sys
from firegaze.util.config import conf

def get_json_data_points(start_date, end_date, query, res, ips):
    '''
    Returns a list of data points between the start and end date for the given query
    '''

    date_to_ts = dateToTs(end_date)
    date_from_ts = dateToTs(start_date)
    
    second_span = date_to_ts - date_from_ts
    span = second_span/res
    
    num = len(ips)
    data_pts = []
    
    for val in xrange(span):
        plot_date = start_date + (timedelta(0, res)*val)
        counts = query(ips[:num], plot_date)
        data_pts.append(counts[0])
    
    return data_pts

class FWLogQuery(object):
    '''
    Provides a common query API for performing specific
    queries on firewall logs.
    
    Each 'highest' method corresponds to a get method, where the get method uses
    a subset of the IPs returned by 'highest'. The querying is much more
    effective in using the subset, since we make use of 'find_one' for each IP
    '''

    def __init__(self, out=False, dbPrefix=''):
        '''
        Initialize a query. Requires a database connection
        '''
        
        self.dbhandler = DBHandler(dbPrefix + conf.DAILY_DB_NAME, 
                                   dbPrefix + conf.MONTHLY_DB_NAME)

        self.out = out
        
        if out:
            self.monthly_col = self.dbhandler.monthly_out_collection
            self.daily_col = self.dbhandler.daily_out_collection
        else:
            self.monthly_col = self.dbhandler.monthly_in_collection
            self.daily_col = self.dbhandler.daily_in_collection

    def most_data_ports(self, date=datetime.utcnow()):
        '''
        Returns a tuple which contains ports and the number of packets for these 
        across all IPs
        '''
        
        now_day = str(date.day)
        day_str = 'daily.'+now_day+'.ports'
        
        results = self.monthly_col.find(
            {'date' : datetime.combine(date, time.min).replace(day=1)},
            {day_str : 1})
        
        dic = {}
        for res in results:
            try:
                for port in res['daily'][now_day]['ports']:
                    if (port != 'total' and port != '-1' and port != '-2'):
                        try:
                            dic[port] += res['daily'][now_day]['ports'][port]
                        except KeyError:
                            dic[port] = res['daily'][now_day]['ports'][port]
            except KeyError:
                sys.stderr.write('Empty result set')
            except TypeError:
                sys.stderr.write('Empty result set')

        sorted_dic = sorted(dic.iteritems(), key=operator.itemgetter(1), 
                            reverse=True) 

        try:     
            ports, counts = zip(*sorted_dic)   
        except ValueError:
            sys.stderr.write('Empty dictionary')
            ports = []
            counts = []
            
        return (ports, counts)

    def highest_day_packet_counts(self, date=datetime.utcnow(), port='total'):
        '''
        Returns tuple which contains IPs and their packet counts for the specified day
        '''
        
        now_day = str(date.day)      
        day_str = 'daily.'+now_day+'.ports'
        
        results = self.monthly_col.find(
            {'date' : datetime.combine(date, time.min).replace(day=1)},
            {day_str : 1, 'IP' : 1}).sort([(day_str+'.'+port, -1)])
        
        ips = []
        counts = []
        
        for res in results:
            try:
                if res['daily'][now_day]['ports']:
                    ips.append(res['IP'])
                    counts.append(res['daily'][now_day]['ports'][port])
            except KeyError:
                sys.stderr.write('Empty result set')
            except TypeError:
                sys.stderr.write('Empty result set')
            
        return (ips, counts) 
    
    def highest_hour_packet_counts(self, date=datetime.utcnow(), port='total'):
        '''
        Returns a tuple which contains IPs and their packet counts for the specified hour of the given day
        '''
        
        now_hour = str(date.hour)
        hour_str = 'hourly.'+now_hour+'.ports'
        
        results = self.daily_col.find(
            {'date' : datetime.combine(date, time.min)},
            {hour_str : 1, 'IP' : 1}).sort([(hour_str+'.'+port, -1)])
        
        ips = []
        counts = []
        
        for res in results:
            try:
                if res['hourly'][now_hour]['ports']:
                    ips.append(res['IP'])
                    counts.append(res['hourly'][now_hour]['ports'][port])
            except KeyError:
                sys.stderr.write('Empty result set')
            except TypeError:
                sys.stderr.write('Empty result set')
            
        return (ips, counts)
    
    def highest_minute_packet_counts(self, date=datetime.utcnow(), port='total'):
        '''
        Returns a tuple which contains IPs and their packet counts for the specified minute of the given day
        '''
        
        now_minute = str(date.minute)
        now_hour = str(date.hour)
        min_str = 'minute.'+now_hour+'.'+now_minute+'.ports'
        
        results = self.daily_col.find(
            {'date': datetime.combine(date, time.min)},
            {min_str : 1, 'IP' : 1}).sort([(min_str+'.'+port, -1)])
        
        ips = []
        counts = []
        
        for res in results:
            try:
                if res['minute'][now_hour][now_minute]['ports']:
                    ips.append(res['IP'])
                    counts.append(res['minute'][now_hour][now_minute]['ports'][port])
            except KeyError:
                sys.stderr.write('Empty result set')
            except TypeError:
                sys.stderr.write('Empty result set')
        
        return (ips, counts)  
    
    def highest_day_port_counts(self, date=datetime.utcnow()):
        '''
        Returns a tuple which contains IPs and their port counts for the specified day of the given date
        '''
        
        now_day = str(date.day)
        day_str = 'daily.'+now_day+'.ports'
        
        results = self.monthly_col.find(
            {'date' : datetime.combine(date, time.min).replace(day=1)},
            {day_str : 1, 'IP' : 1})
        
        dic = {}
        for res in results:
            try:
                if res['daily'][now_day]['ports']:
                    dic[res['IP']] = len(res['daily'][now_day]['ports'].values()) -1 #TODO -1 for 'total' ports, check for other protos
            except KeyError:
                sys.stderr.write('Empty result set')
            except TypeError:
                sys.stderr.write('Empty result set')
        
           
        # cannot sort on ports in query
        sorted_dic = sorted(dic.iteritems(), key=operator.itemgetter(1), 
                            reverse=True) 

        try:     
            ips, counts = zip(*sorted_dic)   
        except ValueError:
            sys.stderr.write('Empty dictionary')
            ips = []
            counts = []
            
        return (ips, counts) 
    
    def highest_hour_port_counts(self, date=datetime.utcnow()):
        '''
        Returns a tuple which contains IPs and their port counts for the specified hour of the given date
        '''
        
        now_hour = str(date.hour)
        hour_str = 'hourly.'+now_hour+'.ports'
        
        results = self.daily_col.find(
            {'date' : datetime.combine(date, time.min)},
            {hour_str : 1, 'IP' : 1})
        
        dic = {}
        for res in results:
            try:
                if res['hourly'][now_hour]['ports']:
                    dic[res['IP']] = len(res['hourly'][now_hour]['ports'].values()) -1 #TODO -1 for 'total' ports, check for other protos
            except KeyError:
                sys.stderr.write('Empty result set')
            except TypeError:
                sys.stderr.write('Empty result set')
        
           
        # cannot sort on ports in query
        sorted_dic = sorted(dic.iteritems(), key=operator.itemgetter(1),
                            reverse=True) 

        try:     
            ips, counts = zip(*sorted_dic)   
        except ValueError:
            sys.stderr.write('Empty dictionary')
            ips = []
            counts = []
            
        return (ips, counts) 

    def highest_minute_port_counts(self, date=datetime.utcnow()):
        '''
        Returns a tuple which contains IPs and their port counts for the specified minute of the given date
        '''
        
        now_minute = str(date.minute)
        now_hour = str(date.hour)
        min_str = 'minute.'+now_hour+'.'+now_minute+'.ports'
        
        results = self.daily_col.find(
            {'date' : datetime.combine(date, time.min)},
            {min_str : 1, 'IP' : 1})
        
        dic = {}
        for res in results:
            try:
                if res['minute'][now_hour][now_minute]['ports']:
                    dic[res['IP']] = len(res['minute'][now_hour][now_minute]['ports'].values()) -1 #TODO -1 for 'total' ports, check for other protos
            except KeyError:
                sys.stderr.write('Empty result set')
            except TypeError:
                sys.stderr.write('Empty result set')
        
           
        # cannot sort on ports in query
        sorted_dic = sorted(dic.iteritems(), key=operator.itemgetter(1),
                            reverse=True) 

        try:     
            ips, counts = zip(*sorted_dic)   
        except ValueError:
            sys.stderr.write('Empty dictionary')
            ips = []
            counts = []
            
        return (ips, counts)    
     
    def get_minute_port_counts(self, ips, date=datetime.utcnow()):
        '''
        Returns port counts for each of the ips in the supplied list, for the given datetime object
        on a minute basis
        '''
        
        now_date = date
        now_hour = str(date.hour)
        now_min = str(date.minute)
        min_str = 'minute.'+now_hour+'.'+now_min+'.ports'
        counts = []
        
        for ip in ips:
            try:
                res = self.daily_col.find_one(
                    {'date' : datetime.combine(now_date, time.min), 'IP' :ip},
                    {min_str : 1, 'IP' : 1})
                
                counts.append(len(res['minute'][now_hour][now_min]['ports'].values()) -1)
            except KeyError:
                counts.append(0)
            except TypeError:
                counts.append(0)
        
        return counts
    
    def get_hour_port_counts(self, ips, date=datetime.utcnow()):
        '''
        Returns port counts for each of the ips in the supplied list, for the given datetime object
        on an hour basis
        '''
        now_date = date
        now_hour = str(date.hour)
        hour_str = 'hourly.'+now_hour+'.ports'
        counts = []
        
        for ip in ips:
            try:
                res = self.daily_col.find_one(
                    {'date' : datetime.combine(now_date, time.min), 'IP' : ip},
                    {hour_str : 1, 'IP' : 1})
                
                counts.append(len(res['hourly'][now_hour]['ports'].values()) -1)
            except KeyError:
                counts.append(0)
            except TypeError:
                counts.append(0)
                
        return counts
    
    def get_day_port_counts(self, ips, date=datetime.utcnow()):
        '''
        Returns port counts for each of the ips in the supplied list, for the given datetime object
        on a daily basis
        '''
        now_date = date
        now_day = str(date.day)
        day_str = 'daily.'+now_day+'.ports'
        counts = []
        
        for ip in ips:
            try:
                res = self.monthly_col.find_one(
                    {'date' : datetime.combine(now_date, time.min).replace(day=1), 'IP' : ip},
                    {day_str : 1, 'IP' : 1})
                
                counts.append(len(res['daily'][now_day]['ports'].values()) -1)
            except KeyError:
                counts.append(0)
            except TypeError:
                counts.append(0)
                
        return counts
                                                                                                                       
    def get_day_packet_counts(self, ips, date=datetime.utcnow(), port='total'):
        '''
        Returns day counts (total or specific port) for each of the 
        ips in the supplied list, for the given datetime object
        '''
        
        now_date = date
        now_day = str(date.day)
        day_str = 'daily.'+now_day+'.ports'
        
        counts = []
        
        for ip in ips:
            try:
                res = self.monthly_col.find_one(
                    {'date' : datetime.combine(now_date, time.min).replace(day=1), 'IP' : ip},
                    {day_str : 1, 'IP' : 1})
                
                counts.append(res['daily'][now_day]['ports'][port])
            except KeyError:
                counts.append(0)
            except TypeError:
                counts.append(0)
                
        return counts
    
    def get_hour_packet_counts(self, ips, date=datetime.utcnow(), port='total'):
        '''
        Returns total hour counts for each of the ips in the supplied list, for the given datetime object
        '''
        
        now_date = date
        now_hour = str(date.hour)
        hour_str = 'hourly.'+now_hour+'.ports'  
        counts = []
        
        for ip in ips:
            try:     
                res = self.daily_col.find_one(
                    {'date': datetime.combine(now_date, time.min), 'IP' : ip},
                    {hour_str : 1, 'IP' : 1})
                
                counts.append(res['hourly'][now_hour]['ports'][port])
            except KeyError:
                counts.append(0)
            except TypeError:
                counts.append(0)
                
        return counts
        
    def get_minute_packet_counts(self, ips, date=datetime.utcnow(), port='total'):
        '''
        Returns total minute counts for each of the ips in the supplied list, for the given datetime object
        '''
        
        now_date = date
        now_hour = str(date.hour)
        now_min = str(date.minute)
        min_str = 'minute.'+now_hour+'.'+now_min+'.ports'
        counts = []
        
        for ip in ips:
            try:
                res = self.daily_col.find_one(
                    {'date' : datetime.combine(now_date, time.min), 'IP' : ip},
                    {min_str : 1, 'IP' : 1})
                
                counts.append(res['minute'][now_hour][now_min]['ports'][port])
            except KeyError:
                counts.append(0)
            except TypeError:
                counts.append(0)
        
        return counts  
    
    def get_ips(self, ips):
        '''
        Returns a list of IPs matching those supplied for the regex expression or space separated IPs
        '''
        
        li = []
        
        for ip in ips:
            result = self.monthly_col.find({'IP' : {'$regex': ip}}, {'IP' : 1})
            for r in result:
                if r['IP'] not in li:
                    li.append(r['IP'])
                

        return li
    
    def get_minute_packet_counts_by_port(self, ip, date=datetime.utcnow()): 
        '''
        Returns a dictionary with ports as keys and counts as values per minute
        '''
        
        now_minute = str(date.minute)
        now_hour = str(date.hour)
        min_str = 'minute.'+now_hour+'.'+now_minute+'.ports'
        
        result = self.daily_col.find_one(
            {'date': datetime.combine(date, time.min), 'IP' : ip},
            {min_str : 1, 'IP' : 1})
        
        port_counts = {}
        
        try: 
            for port in result['minute'][now_hour][now_minute]['ports']:
                port_counts[port] = result['minute'][now_hour][now_minute]['ports'][port]
        except KeyError:
            sys.stderr.write('No entry for this minute')
            port_counts['total'] = 0
        except TypeError:
            sys.stderr.write('Empty result set')
            port_counts['total'] = 0
            
        port_counts_sorted = sorted(port_counts.iteritems(), 
                                    key=operator.itemgetter(0))
            
        return port_counts_sorted
    
    def get_hour_packet_counts_by_port(self, ip, date=datetime.utcnow()): 
        '''
        Returns a dictionary with ports as keys and counts as values per hour
        '''
        
        now_hour = str(date.hour)
        hour_str = 'hourly.'+now_hour+'.ports'
        
        result = self.daily_col.find_one(
            {'date': datetime.combine(date, time.min), 'IP' : ip},
            {hour_str : 1, 'IP' : 1})
        
        port_counts = {}
        try:
            for port in result['hourly'][now_hour]['ports']:
                port_counts[port] = result['hourly'][now_hour]['ports'][port]
        except KeyError:
            sys.stderr.write('No entry for this minute')
            port_counts['total'] = 0
        except TypeError:
            sys.stderr.write('Empty result set')
            port_counts['total'] = 0
            
        port_counts_sorted = sorted(port_counts.iteritems(),
                                    key=operator.itemgetter(0))
            
        return port_counts_sorted
    
    def get_day_packet_counts_by_port(self, ip, date=datetime.utcnow()): 
        '''
        Returns a list of tuples with (port, counts per day)
        '''
        
        now_day = str(date.day)
        day_str = 'daily.'+now_day+'.ports'
        
        result = self.monthly_col.find_one(
            {'date': datetime.combine(date, time.min).replace(day=1), 'IP' : ip},
            {day_str : 1, 'IP' : 1})
        
        port_counts = {}
        try:
            for port in result['daily'][now_day]['ports']:
                port_counts[port] = result['daily'][now_day]['ports'][port]
        except KeyError:
            sys.stderr.write('No entry for this minute')
            port_counts['total'] = 0
        except TypeError:
            sys.stderr.write('Empty result set')
            port_counts['total'] = 0
            
        port_counts_sorted = sorted(port_counts.iteritems(), 
                                    key=operator.itemgetter(0))
            
        return port_counts_sorted
    
    def get_minute_packet_counts_by_srcs(self, ip, date=datetime.utcnow()):
        '''
        Returns a list of tuples with (src ips, counts per minute)
        '''
        
        now_minute = str(date.minute)
        now_hour = str(date.hour)
        min_str = 'minute.'+now_hour+'.'+now_minute+'.srcs'
        
        result = self.daily_col.find_one(
            {'date': datetime.combine(date, time.min), 'IP' : ip},
            {min_str : 1, 'IP' : 1})
        
        srcs_counts = {}
        try:
            for src_ip in result['minute'][now_hour][now_minute]['srcs']:
                srcs_counts[src_ip] = result['minute'][now_hour][now_minute]['srcs'][src_ip]
        except KeyError:
            sys.stderr.write('No entry for this minute')
            srcs_counts['total'] = 0
        except TypeError:
            sys.stderr.write('Empty result set')
            srcs_counts['total'] = 0
            
        srcs_counts_sorted = sorted(srcs_counts.iteritems(),
                                    key=operator.itemgetter(0))
            
        return srcs_counts_sorted
    
    def get_hour_packet_counts_by_srcs(self, ip, date=datetime.utcnow()):
        '''
        Returns a list of tuples with (src ips, counts per hour)
        '''
        
        now_hour = str(date.hour)
        hour_str = 'hourly.'+now_hour+'.srcs'
        
        result = self.daily_col.find_one(
            {'date': datetime.combine(date, time.min), 'IP' : ip},
            {hour_str : 1, 'IP' : 1})
        
        srcs_counts = {}
        try:
            for src_ip in result['hourly'][now_hour]['srcs']:
                srcs_counts[src_ip] = result['hourly'][now_hour]['srcs'][src_ip]
        except KeyError:
            sys.stderr.write('No entry for this hour')
            srcs_counts['total'] = 0
        except TypeError:
            sys.stderr.write('Empty result set')
            srcs_counts['total'] = 0
            
        srcs_counts_sorted = sorted(srcs_counts.iteritems(),
                                    key=operator.itemgetter(0))
            
        return srcs_counts_sorted
    
    def get_day_packet_counts_by_srcs(self, ip, date=datetime.utcnow()):
        '''
        Returns a list of tuples with (src ips, counts per day)
        '''

        now_day = str(date.day)
        day_str = 'daily.'+now_day+'.srcs'
        
        result = self.monthly_col.find_one(
            {'date': datetime.combine(date, time.min).replace(day=1), 'IP' : ip},
            {day_str : 1, 'IP' : 1})
        
        srcs_counts = {}
        try:
            for src_ip in result['daily'][now_day]['srcs']:
                srcs_counts[src_ip] = result['daily'][now_day]['srcs'][src_ip]
        except KeyError:
            sys.stderr.write('No entry for this day')
            srcs_counts['total'] = 0
        except TypeError:
            sys.stderr.write('Empty result set')
            srcs_counts['total'] = 0
            
        srcs_counts_sorted = sorted(srcs_counts.iteritems(),
                                    key=operator.itemgetter(0))
            
        return srcs_counts_sorted
        
def dateToTs(date):
    '''
    Convert datetime object to timestamp
    '''
  
    return int(t.mktime(date.timetuple()))